const gameScreen = document.querySelector("#game-screen")
const btnDiv = document.querySelector("#btn-div")
const startBtn = document.querySelector(".start-btn")
const SQUARES_NUMBER = 10
const timeForEasyLvl = 1500
const timeForMediumLvl = 1000
const timeForHardLvl = 500

class Cell {
    constructor(width, height) {
        this.width = width;
        this.height = height;
    }

    tableElementCreate() {
        const element = document.createElement("td")
        element.style.width = this.width
        element.style.height = this.height
        element.style.backgroundColor = "#FFFFFF"
        element.style.border = "1px solid black"
        element.classList.add("square")

        return element
    }
}

const tableElement = new Cell("40px", "40px")

for (let i = 0; i < SQUARES_NUMBER; i++) {
    let tr = document.createElement("tr")

    for (let i = 0; i < SQUARES_NUMBER; i++) {
        let td = tableElement.tableElementCreate()

        tr.append(td)
    }

    gameScreen.append(tr)
}

const tdArray = Array.from(document.querySelectorAll(".square"))
let randomNumArray = []

while (randomNumArray.length < 100) {
    let randomNumber = Math.ceil(Math.random() * 100);
    let found = false;
    for (let i = 0; i < randomNumArray.length; i++) {
        if (randomNumArray[i] === randomNumber) {
            found = true;
            break;
        }
    }
    if (!found) {
        randomNumArray[randomNumArray.length] = randomNumber;
    }
}

for (let i = 0; i < 100; i++) {
    tdArray[i].style.order = `${randomNumArray[i]}`;
    tdArray[i].setAttribute('data-index-number', randomNumArray[i])
}

tdArray.sort(function (a, b) {
    if (+a.dataset.indexNumber > +b.dataset.indexNumber) {
        return 1
    }
    if (+a.dataset.indexNumber < +b.dataset.indexNumber) {
        return -1
    }
    return 0
})

let setColor = function (e) {
    console.log(e.target)
    e.target.style.backgroundColor = "green"
}

let timeGame = timeForMediumLvl

function styleReset(e) {
    Array.from(e.target.parentElement.children).forEach(i => {
        i.style.backgroundColor = "darkcyan"
    })
}

let setTime = function (e) {
    if (e.target.dataset.btn === "1") {
        styleReset(e)
        e.target.style.backgroundColor = "green"
        return timeGame = timeForEasyLvl
    } else if (e.target.dataset.btn === "2") {
        styleReset(e)
        e.target.style.backgroundColor = "green"
        return timeGame = timeForMediumLvl
    } else if (e.target.dataset.btn === "3") {
        styleReset(e)
        e.target.style.backgroundColor = "green"
        return timeGame = timeForHardLvl
    }
}

function gameFunction(timeLvl) {
    let time = timeLvl
    let counter = 0
    let greenCounter = 0
    let redCounter = 0
    let howManyTimes = tdArray.length + 1;

    function f() {

        tdArray.forEach(i => {
            if (Number.parseInt(i.dataset.indexNumber) - 1 === counter) {
                i.style.backgroundColor = "blue"
                i.addEventListener("click", setColor, true)
            } else {
                i.removeEventListener("click", setColor, true)
            }

            if (i.dataset.indexNumber - 1 < counter && i.style.backgroundColor === "blue") {
                i.style.backgroundColor = "red"
            }

        })


        tdArray.forEach(i => {

            if (i.style.backgroundColor === "green") {
                greenCounter++
            } else if (i.style.backgroundColor === "red") {
                redCounter++
            }
        })
        if (greenCounter > 50) {
            alert("Вы выиграли")
            return
        } else if (redCounter > 50) {
            alert("Вы проиграли")
            return
        } else if (redCounter === greenCounter && redCounter === SQUARES_NUMBER * SQUARES_NUMBER / 2) {
            alert("Ничья")
            return
        }

        greenCounter = 0
        redCounter = 0
        counter++;

        if (counter < howManyTimes) {
            setTimeout(f, time);
        }
    }

    f();
}

btnDiv.addEventListener("click", setTime)

let timer
let x = 3

function countdown() {
    document.querySelector('.countdown').innerHTML = `${x}`
    x--
    if (x < 0) {
        document.querySelector('.countdown').innerHTML = ""
        clearTimeout(timer)
    } else {
        timer = setTimeout(countdown, 1000);
    }
}

startBtn.addEventListener("click", () => {
    setTimeout("gameFunction(timeGame)", 3000)
    countdown()
}, {once: true})
